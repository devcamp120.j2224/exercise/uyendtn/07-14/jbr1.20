public class Rectangle {
    float length = 1.0f;
    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    float width = 1.0f;
    
    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }

    public Rectangle() {
    }

    public double getArea() {
        return length*width;
    }

    public double getPerimeter() {
        return (length + width)*2;
    }

    public String toString() {
        return "Rectangle [width = " + width + "; length = " + length + "]";
    }

}
