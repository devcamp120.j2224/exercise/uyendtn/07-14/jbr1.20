public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.0f, 3.0f);

        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());

        //area and perimeter of rectangle 1
        System.out.println("area of rectangle 1: " + rectangle1.getArea());
        System.out.println("perimeter of rectangle 1: " + rectangle1.getPerimeter());

         //area and perimeter of rectangle 2
         System.out.println("area of rectangle 2: " + rectangle2.getArea());
         System.out.println("perimeter of rectangle 2: " + rectangle2.getPerimeter());
 

    }
}
